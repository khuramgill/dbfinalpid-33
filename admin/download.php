<?php
// Include connection file if needed


// Function to download PDF for total blood donors
function downloadTotalBloodDonorsPDF() {
    include '../conn.php';
    // Include the library
    require_once('fpdf/fpdf.php');

    // Placeholder code to generate PDF for total blood donors
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',12);

    // Title
    $pdf->Cell(0, 10, 'Total Blood Donors Report', 0, 1, 'C');
    $pdf->Ln(10);

    // Header
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(40, 10, 'Name', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Phone', 1, 0, 'C');
    $pdf->Cell(70, 10, 'Address', 1, 0, 'C');
    $pdf->Cell(20, 10, 'Age', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Gender', 1, 1, 'C');
    
    // Data
    $pdf->SetFont('Arial', '', 12);
    $sql = "SELECT * FROM view_blood_donors";
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $pdf->Cell(40, 10, $row["name"], 1, 0);
            $pdf->Cell(30, 10, $row["phone"], 1, 0);
            $pdf->Cell(70, 10, $row["address"], 1, 0);
            $pdf->Cell(20, 10, $row["age"], 1, 0);
            $pdf->Cell(30, 10, $row["gender"], 1, 1);
        }
    } else {
        $pdf->Cell(0, 10, 'No donors found', 1, 1, 'C');
    }

    // Output PDF
    $pdf->Output();
}


// Function to download PDF for total money donors
function downloadTotalMoneyDonorsPDF() {
    include '../conn.php';
    // Include the library
    require_once('fpdf/fpdf.php');

    // Placeholder code to generate PDF for total blood donors
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',12);

    // Title
    $pdf->Cell(0, 10, 'Total Money Donors Report', 0, 1, 'C');
    $pdf->Ln(10);

    // Header
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(40, 10, 'Name', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Phone', 1, 0, 'C');
    $pdf->Cell(70, 10, 'Address', 1, 0, 'C');
    $pdf->Cell(20, 10, 'CNIC', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Gender', 1, 1, 'C');
    $pdf->Cell(30, 10, 'Amount', 1, 1, 'C');
    
    // Data
    $pdf->SetFont('Arial', '', 12);
    $sql = "SELECT * FROM view_money_donors";
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $pdf->Cell(40, 10, $row["name"], 1, 0);
            $pdf->Cell(30, 10, $row["phone"], 1, 0);
            $pdf->Cell(70, 10, $row["address"], 1, 0);
            $pdf->Cell(20, 10, $row["CNIC"], 1, 0);
            $pdf->Cell(30, 10, $row["gender"], 1, 1);
            $pdf->Cell(40, 10, $row["amount"], 1, 1);
        }
    } else {
        $pdf->Cell(0, 10, 'No donors found', 1, 1, 'C');
    }

    // Output PDF
    $pdf->Output();
}

// Function to download PDF for users who are both blood and money donors
function downloadUsersBothDonorsPDF() {
    include '../conn.php';
    // Include the library
    require_once('fpdf/fpdf.php');

    // Placeholder code to generate PDF for total blood donors
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',12);

    // Title
    $pdf->Cell(0, 10, 'Both Donors Report', 0, 1, 'C');
    $pdf->Ln(10);

    // Header
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(40, 10, 'Name', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Phone', 1, 0, 'C');
    $pdf->Cell(20, 10, 'Age', 1, 1, 'C');
    $pdf->Cell(30, 10, 'Blood Group', 1, 1, 'C');
    $pdf->Cell(20, 10, 'CNIC', 1, 0, 'C');
    $pdf->Cell(40, 10, 'Amount', 1, 1, 'C');
    $pdf->Cell(20, 10, 'Gender', 1, 1, 'C');
    $pdf->Cell(70, 10, 'Address', 1, 0, 'C');
    
    
    // Data
    $pdf->SetFont('Arial', '', 12);
    $sql = "SELECT * FROM view_both_donors";
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $pdf->Cell(40, 10, $row["name"], 1, 0);
            $pdf->Cell(30, 10, $row["phone"], 1, 0);
            $pdf->Cell(20, 10, $row["age"], 1, 0);
            $pdf->Cell(30, 10, $row["blood_group"], 1, 0);
            $pdf->Cell(20, 10, $row["CNIC"], 1, 0);
            $pdf->Cell(40, 10, $row["amount"], 1, 1);
            $pdf->Cell(20, 10, $row["gender"], 1, 1);
            $pdf->Cell(70, 10, $row["address"], 1, 0);
        }
    } else {
        $pdf->Cell(0, 10, 'No donors found', 1, 1, 'C');
    }

    // Output PDF
    $pdf->Output();
}

// Function to download PDF for system users
function downloadSystemUsersPDF() {
    include '../conn.php';
    // Include the library
    require_once('fpdf/fpdf.php');

    // Placeholder code to generate PDF for total blood donors
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',12);

    // Title
    $pdf->Cell(0, 10, 'System Users Report', 0, 1, 'C');
    $pdf->Ln(10);

    // Header
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(40, 10, 'Name', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Email', 1, 0, 'C');
    
    
    // Data
    $pdf->SetFont('Arial', '', 12);
    $sql = "SELECT * FROM view_users";
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $pdf->Cell(40, 10, $row["username"], 1, 0);
            $pdf->Cell(30, 10, $row["email"], 1, 0);
        }
    } else {
        $pdf->Cell(0, 10, 'No donors found', 1, 1, 'C');
    }

    // Output PDF
    $pdf->Output();
}

// Function to download PDF for users who are not donors
function downloadUsersNotBloodDonorsPDF() {
    include '../conn.php';
    // Include the library
    require_once('fpdf/fpdf.php');

    // Placeholder code to generate PDF for total blood donors
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',12);

    // Title
    $pdf->Cell(0, 10, 'Users Not Blood Donors Report', 0, 1, 'C');
    $pdf->Ln(10);

    // Header
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(40, 10, 'Name', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Phone', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Gender', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Age', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Address', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Blood Group', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Medical History', 1, 0, 'C');
    
    
    // Data
    $pdf->SetFont('Arial', '', 12);
    $sql = "SELECT * FROM view_nonuser_blooddonors";
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $pdf->Cell(40, 10, $row["name"], 1, 0);
            $pdf->Cell(30, 10, $row["phone"], 1, 0);
            $pdf->Cell(30, 10, $row["gender"], 1, 0);
            $pdf->Cell(30, 10, $row["age"], 1, 0);
            $pdf->Cell(30, 10, $row["address"], 1, 0);
            $pdf->Cell(30, 10, $row["blood_group"], 1, 0);
            $pdf->Cell(30, 10, $row["history_description"], 1, 0);
        }
    } else {
        $pdf->Cell(0, 10, 'No donors found', 1, 1, 'C');
    }

    // Output PDF
    $pdf->Output();
}

function downloadUsersNotMoneyDonorsPDF(){
    include '../conn.php';
    // Include the library
    require_once('fpdf/fpdf.php');

    // Placeholder code to generate PDF for total blood donors
    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',12);

    // Title
    $pdf->Cell(0, 10, 'Non User Money Donors Report', 0, 1, 'C');
    $pdf->Ln(10);

    // Header
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell(40, 10, 'Name', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Phone', 1, 0, 'C');
    $pdf->Cell(70, 10, 'Address', 1, 0, 'C');
    $pdf->Cell(20, 10, 'CNIC', 1, 0, 'C');
    $pdf->Cell(30, 10, 'Gender', 1, 1, 'C');
    $pdf->Cell(30, 10, 'Amount', 1, 1, 'C');
    
    // Data
    $pdf->SetFont('Arial', '', 12);
    $sql = "SELECT * FROM view_nonuser_moneydonors";
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $pdf->Cell(40, 10, $row["name"], 1, 0);
            $pdf->Cell(30, 10, $row["phone"], 1, 0);
            $pdf->Cell(70, 10, $row["address"], 1, 0);
            $pdf->Cell(20, 10, $row["CNIC"], 1, 0);
            $pdf->Cell(30, 10, $row["gender"], 1, 1);
            $pdf->Cell(40, 10, $row["amount"], 1, 1);
        }
    } else {
        $pdf->Cell(0, 10, 'No donors found', 1, 1, 'C');
    }

    // Output PDF
    $pdf->Output();
}

// Check if a specific download function is called
if(isset($_GET['action'])) {
    $action = $_GET['action'];
    switch($action) {
        case 'downloadTotalBloodDonorsPDF':
            downloadTotalBloodDonorsPDF();
            break;
        case 'downloadTotalMoneyDonorsPDF':
            downloadTotalMoneyDonorsPDF();
            break;
        case 'downloadUsersBothDonorsPDF':
            downloadUsersBothDonorsPDF();
            break;
        case 'downloadSystemUsersPDF':
            downloadSystemUsersPDF();
            break;
        case 'downloadUsersNotBloodDonorsPDF':
            downloadUsersNotBloodDonorsPDF();
            break;
        case 'downloadUsersNotMoneyDonorsPDF':
            downloadUsersNotMoneyDonorsPDF();
            break;
        default:
            // Handle invalid action
            echo "Invalid action!";
            break;
    }
}
?>
