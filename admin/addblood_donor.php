<?php
include 'session.php';

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['submit'])) {
    include '../conn.php';

    // Retrieve form data
    $fullname = $_POST['fullname'];
    $mobileno = $_POST['mobileno'];
    $medical = $_POST['medical'];
    $age = $_POST['age'];
    $gender = $_POST['gender'];
    $blood_type_id = $_POST['blood'];
    $address = $_POST['address'];

    // Insert contact information
    $insertContactInformation = "INSERT INTO contactinformation (phone, address, user_id) VALUES ('$mobileno', '$address', Null ) ";
    if (mysqli_query($conn, $insertContactInformation)) {
        // Retrieve the ID of the inserted contact information
        $contactInfoId = mysqli_insert_id($conn);
    } else {
        echo "Error: " . $insertContactInformation . "<br>" . mysqli_error($conn);
        exit; // Stop execution if an error occurs
    }

    // Insert medical history
    $insertMedicalSql = "INSERT INTO medicalhistory (history_description, user_id) VALUES ('$medical', null)";
    if (mysqli_query($conn, $insertMedicalSql)) {
        // Retrieve the ID of the inserted medical history
        $medicalHistoryId = mysqli_insert_id($conn);

        // Construct the SQL INSERT query for 'blood_donors' table
        $sql = "INSERT INTO blood_donors (name, age, gender, blood_type_id, contact_info_id, medical_history_id, user_id)
                VALUES ('$fullname', '$age', '$gender', '$blood_type_id', '$contactInfoId', '$medicalHistoryId', null )";

        if (mysqli_query($conn, $sql)) {
            // Redirect to blooddonor_list.php after successful insertion
            header("Location: blooddonor_list.php");
            exit; // Stop execution after redirection
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
    } else {
        echo "Error: " . $insertMedicalSql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn); // Close the connection
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <style>
        #sidebar {
            position: relative;
            margin-top: -20px
        }

        #content {
            position: relative;
            margin-left: 210px
        }

        @media screen and (max-width: 600px) {
            #content {
                position: relative;
                margin-left: auto;
                margin-right: auto;
            }
        }
    </style>
</head>

<body style="color:black">
    <?php
    include 'conn.php';
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
    ?>
        <div id="header">
            <?php $active = "addblood";
            include 'header.php';
            ?>
        </div>
        <div id="sidebar">
            <?php include 'sidebar.php'; ?>

        </div>
        <div id="content">
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 lg-12 sm-12">

                            <h1 class="page-title">Add Donor</h1>
                        </div>
                    </div>
                    <hr>
                    <form name="donor" action="" method="post">
                        <div class="row">
                            <div class="col-lg-4 mb-4"><br>
                                <div class="font-italic">Full Name<span style="color:red">*</span></div>
                                <div><input type="text" name="fullname" class="form-control" required></div>
                            </div>
                            <div class="col-lg-4 mb-4"><br>
                                <div class="font-italic">Mobile Number<span style="color:red">*</span></div>
                                <div><input type="text" name="mobileno" class="form-control" required></div>
                            </div>
                            <div class="col-lg-4 mb-4"><br>
                                <div class="font-italic">Medical History</div>
                                <div><input type="medical" name="medical" class="form-control"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 mb-4"><br>
                                <div class="font-italic">Age<span style="color:red">*</span></div>
                                <div><input type="text" name="age" class="form-control" required></div>
                            </div>


                            <div class="col-lg-4 mb-4"><br>
                                <div class="font-italic">Gender<span style="color:red">*</span></div>
                                <div><select name="gender" class="form-control" required>
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 mb-4"><br>
                                <div class="font-italic">Blood Group<span style="color:red">*</span></div>
                                <div><select name="blood" class="form-control" required>
                                        <option value="" selected disabled>Select</option>
                                        <?php
                                        include 'conn.php';
                                        $sql = "select * from blood";
                                        $result = mysqli_query($conn, $sql) or die("query unsuccessful.");
                                        while ($row = mysqli_fetch_assoc($result)) {
                                        ?>
                                            <option value=" <?php echo $row['blood_id'] ?>"> <?php echo $row['blood_group'] ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-4 mb-4">
                                <div class="font-italic">Address<span style="color:red">*</span></div>
                                <div><textarea class="form-control" name="address" required></textarea></div>
                            </div>
                        </div> <br>
                        <div class="row">
                            <div class="col-lg-4 mb-4">
                                <div><input type="submit" name="submit" class="btn btn-primary" value="Submit" style="cursor:pointer"></div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    <?php
    } else {
        echo '<div class="alert alert-danger"><b> Please Login First To Access Admin Portal.</b></div>';
        ?>
        <form method="post" name="" action="login.php" class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-4" style="float:left">

                    <button class="btn btn-primary" name="submit" type="submit">Go to Login Page</button>
                </div>
            </div>
        </form>
    <?php }
    ?>
    <script>
        function popup() {
            alert("Data added Successfully.");
        }
    </script>
</body>

</html>
