<?php
// Include database connection
include '../conn.php';

// Check if the moneydonor_id is provided via GET request
if(isset($_GET['moneydonor_id'])) {
    // Sanitize the moneydonor_id to prevent SQL injection
    $moneydonor_id = mysqli_real_escape_string($conn, $_GET['moneydonor_id']);

    // Fetch the contact_info_id associated with the moneydonor_id
    $sql3 = "SELECT contact_info_id FROM moneydonor WHERE moneydonor_id = '$moneydonor_id'";
    $result3 = mysqli_query($conn, $sql3);
    if ($result3) {
        $row = mysqli_fetch_assoc($result3);
        $contact_info_id = $row['contact_info_id'];

        // Construct the DELETE query for moneydonor and contactinformation tables
        $sql = "DELETE FROM moneydonor WHERE moneydonor_id = '$moneydonor_id'";
        $sql2 = "DELETE FROM contactinformation WHERE contact_info_id = '$contact_info_id'";

        // Execute the DELETE queries
        $result = mysqli_query($conn, $sql);
        $result2 = mysqli_query($conn, $sql2);

        // Check if deletion was successful
        if($result && $result2) {
            // Redirect back to the fund donor list page after successful deletion
            header("Location: funddonor_list.php");
            exit;
        } else {
            // Display an error message if deletion fails
            echo "Error deleting record: " . mysqli_error($conn);
        }
    } else {
        // Display an error message if the query fails
        echo "Error: " . mysqli_error($conn);
    }
}

// Close database connection
mysqli_close($conn);
?>
