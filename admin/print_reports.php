<html>
    <head>
        <style>
            #sidebar{position:relative;margin-top:-20px}
            #content{position:relative;margin-left:210px}
            @media screen and (max-width: 600px) {
            #content {
                position:relative;margin-left:auto;margin-right:auto;
            }
            }


            .block-anchor {
                    color:red;
                    cursor: pointer;
                }
        </style>
    </head>
    <body style="color:black;" >

        <?php
        include 'conn.php';
        include 'session.php';
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        ?>

            <div id="header">
                <?php include 'header.php';?>
            </div>
            <div id="sidebar">
                <?php $active="report"; include 'sidebar.php'; ?>
            </div>
            <div id="content">

                <div class="content-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 lg-12 sm-12">
                                <h1 class="page-title">Print Reports</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-default panel-info" style="border-radius:50px;">
                            <div class="panel-body panel-info bk-primary text-light" style="background-color:#D6EAF8; border-radius:50px">
                            <div class="stat-panel text-center">
                                    <button class="btn btn-danger" onclick="window.location.href = 'download.php?action=downloadTotalBloodDonorsPDF';">
                                    Download PDF<i class="fa fa-arrow-right"></i>
                                    </button>


                                    <div class="stat-panel-number h1"></div>
                                    <div class="stat-panel-title text-uppercase">Total Blood Donors</div>
                                    <br>
                                    <button class="btn btn-danger" onclick="window.location.href = 'view.php?action=viewTotalBloodDonors';">
                                        View Record <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="panel panel-default panel-info" style="border-radius:50px;">
                            <div class="panel-body panel-info bk-primary text-light" style="background-color:#D6EAF8; border-radius:50px">
                            <div class="stat-panel text-center">
                                    <button class="btn btn-danger" onclick="window.location.href = 'download.php?action=downloadTotalMoneyDonorsPDF';">
                                    Download PDF<i class="fa fa-arrow-right"></i>
                                    </button>


                                    <div class="stat-panel-number h1"></div>
                                    <div class="stat-panel-title text-uppercase">Total Money Donors</div>
                                    <br>
                                    <button class="btn btn-danger" onclick="window.location.href = 'view.php?action=viewTotalMoneyDonors';">
                                        View Record <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-default panel-info" style="border-radius:50px;">
                            <div class="panel-body panel-info bk-primary text-light" style="background-color:#D6EAF8; border-radius:50px">
                            <div class="stat-panel text-center">
                                    <button class="btn btn-danger" onclick="window.location.href = 'download.php?action=downloadUsersBothDonorsPDF';">
                                    Download PDF<i class="fa fa-arrow-right"></i>
                                    </button>


                                    <div class="stat-panel-number h1"></div>
                                    <div class="stat-panel-title text-uppercase">Users Who Are Both Donors</div>
                                    <br>
                                    <button class="btn btn-danger" onclick="window.location.href = 'view.php?action=viewUsersBothDonors';">
                                        View Record <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>

                
                <div class="col-md-3">
                    <div class="panel panel-default panel-info" style="border-radius:50px;">
                            <div class="panel-body panel-info bk-primary text-light" style="background-color:#D6EAF8; border-radius:50px">
                            <div class="stat-panel text-center">
                                    <button class="btn btn-danger" onclick="window.location.href = 'download.php?action=downloadSystemUsersPDF';">
                                    Download PDF<i class="fa fa-arrow-right"></i>
                                    </button>


                                    <div class="stat-panel-number h1"></div>
                                    <div class="stat-panel-title text-uppercase">System Users</div>
                                    <br>
                                    <button class="btn btn-danger" onclick="window.location.href = 'view.php?action=viewSystemUsers';">
                                        View Record <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-default panel-info" style="border-radius:50px;">
                            <div class="panel-body panel-info bk-primary text-light" style="background-color:#D6EAF8; border-radius:50px">
                            <div class="stat-panel text-center">
                                    <button class="btn btn-danger" onclick="window.location.href = 'download.php?action=downloadUsersNotBloodDonorsPDF';">
                                    Download PDF<i class="fa fa-arrow-right"></i>
                                    </button>


                                    <div class="stat-panel-number h1"></div>
                                    <div class="stat-panel-title text-uppercase">Users Who Are Not Blood Donors</div>
                                    <br>
                                    <button class="btn btn-danger" onclick="window.location.href = 'view.php?action=viewUsersNotBloodDonors';">
                                        View Record <i class="fa fa-arrow-right"></i>
                                    </button>

                                </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="panel panel-default panel-info" style="border-radius:50px;">
                            <div class="panel-body panel-info bk-primary text-light" style="background-color:#D6EAF8; border-radius:50px">
                            <div class="stat-panel text-center">
                                    <button class="btn btn-danger" onclick="window.location.href = 'download.php?action=downloadUsersNotMoneyDonorsPDF';">
                                    Download PDF<i class="fa fa-arrow-right"></i>
                                    </button>


                                    <div class="stat-panel-number h1"></div>
                                    <div class="stat-panel-title text-uppercase">Users Who Are Not Money Donors</div>
                                    <br>
                                    <button class="btn btn-danger" onclick="window.location.href = 'view.php?action=viewUsersNotMoneyDonors';">
                                        View Record <i class="fa fa-arrow-right"></i>
                                    </button>

                                </div>
                            </div>
                    </div>
                </div>


            </div>
        <?php }?>
    </body>
</html>
