<html>
    <head>
        <style>
            #sidebar{position:relative;margin-top:-20px}
            #content{position:relative;margin-left:210px}
            @media screen and (max-width: 600px) {
            #content {
                position:relative;margin-left:auto;margin-right:auto;
            }
            }


            .block-anchor {
                    color:red;
                    cursor: pointer;
                }
        </style>
    </head>
    <body style="color:black;" >

        <?php
        include 'conn.php';
        include 'session.php';
        if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
        ?>

            <div id="header">
                <?php include 'header.php';?>
            </div>
            <div id="sidebar">
                <?php $active="report"; include 'sidebar.php'; ?>
            </div>
            <div id="content">

                <div class="content-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 lg-12 sm-12">
                                <h1 class="page-title">Print Reports</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }?>
    </body>
</html>

<?php
// Include connection file if needed
include '../conn.php';

// Function to view total blood donors
function viewTotalBloodDonors() {
    include '../conn.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    // Query to fetch data from the view
    $sql = "SELECT * FROM view_blood_donors";

    // Perform the query
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        // Output table header
        echo "<div class='container'>
                <div class='table-responsive'>
                    <table class='table table-striped table-bordered'>
                        <thead class='thead-dark'>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Age</th>
                                <th>Gender</th>
                                <th>Blood Group</th>
                                <th>Medical History</th>
                            </tr>
                        </thead>
                        <tbody>";

        // Output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row["name"]. "</td>";
            echo "<td>" . $row["phone"]. "</td>";
            echo "<td>" . $row["address"]. "</td>";
            echo "<td>" . $row["age"]. "</td>";
            echo "<td>" . $row["gender"]. "</td>";
            echo "<td>" . $row["blood_group"]. "</td>";
            echo "<td>" . $row["history_description"]. "</td>";
            echo "</tr>";
        }
        echo "</tbody></table></div></div>";
    } else {
        echo "No donors found";
    }

    // Close the connection
    mysqli_close($conn);
}

// Function to view total money donors
function viewTotalMoneyDonors() {
    include '../conn.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    // Query to fetch data from the view
    $sql = "SELECT * FROM view_money_donors";

    // Perform the query
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        // Output table header
        echo "<div class='container'>
                <div class='table-responsive'>
                    <table class='table table-striped table-bordered'>
                        <thead class='thead-dark'>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Gender</th>
                                <th>CNIC</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>";

        // Output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row["name"]. "</td>";
            echo "<td>" . $row["phone"]. "</td>";
            echo "<td>" . $row["address"]. "</td>";
            echo "<td>" . $row["gender"]. "</td>";
            echo "<td>" . $row["CNIC"]. "</td>";
            echo "<td>" . $row["amount"]. "</td>";
            echo "</tr>";
        }
        echo "</tbody></table></div></div>";
    } else {
        echo "No donors found";
    }

    // Close the connection
    mysqli_close($conn);
}

// Function to view users who are both blood and money donors
function viewUsersBothDonors() {
    include '../conn.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    // Query to fetch data from the view
    $sql = "SELECT * FROM view_both_donors";

    // Perform the query
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        // Output table header
        echo "<div class='container'>
                <div class='table-responsive'>
                    <table class='table table-striped table-bordered'>
                        <thead class='thead-dark'>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Gender</th>
                                <th>Age</th>
                                <th>CNIC</th>
                                <th>Blood Group</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>";

        // Output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row["name"]. "</td>";
            echo "<td>" . $row["phone"]. "</td>";
            echo "<td>" . $row["address"]. "</td>";
            echo "<td>" . $row["gender"]. "</td>";
            echo "<td>" . $row["age"]. "</td>";
            echo "<td>" . $row["CNIC"]. "</td>";
            echo "<td>" . $row["blood_group"]. "</td>";
            echo "<td>" . $row["amount"]. "</td>";
            echo "</tr>";
        }
        echo "</tbody></table></div></div>";
    } else {
        echo "No donors found";
    }

    // Close the connection
    mysqli_close($conn);
}

// Function to view system users
function viewSystemUsers() {
    include '../conn.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    // Query to fetch data from the view
    $sql = "SELECT * FROM view_users";

    // Perform the query
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        // Output table header
        echo "<div class='container'>
                <div class='table-responsive'>
                    <table class='table table-striped table-bordered'>
                        <thead class='thead-dark'>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>";

        // Output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row["username"]. "</td>";
            echo "<td>" . $row["email"]. "</td>";
            echo "</tr>";
        }
        echo "</tbody></table></div></div>";
    } else {
        echo "No user found";
    }

    // Close the connection
    mysqli_close($conn);
}

// Function to view users who are not donors
function viewUsersNotBloodDonors() {
    include '../conn.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    // Query to fetch data from the view
    $sql = "SELECT * FROM view_nonUser_blooddonors";

    // Perform the query
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        // Output table header
        echo "<div class='container'>
                <div class='table-responsive'>
                    <table class='table table-striped table-bordered'>
                        <thead class='thead-dark'>
                            <tr>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Address</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Blood Group</th>
                            <th>Medical History</th>
                            </tr>
                        </thead>
                        <tbody>";

        // Output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row["name"]. "</td>";
            echo "<td>" . $row["phone"]. "</td>";
            echo "<td>" . $row["address"]. "</td>";
            echo "<td>" . $row["age"]. "</td>";
            echo "<td>" . $row["gender"]. "</td>";
            echo "<td>" . $row["blood_group"]. "</td>";
            echo "<td>" . $row["history_description"]. "</td>";
            echo "</tr>";
        }
        echo "</tbody></table></div></div>";
    } else {
        echo "No donor found";
    }

    // Close the connection
    mysqli_close($conn);
}

function viewUsersNotMoneyDonors() {
    include '../conn.php';
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    // Query to fetch data from the view
    $sql = "SELECT * FROM view_nonUser_moneydonors";

    // Perform the query
    $result = mysqli_query($conn, $sql);

    // Check if there are any records
    if (mysqli_num_rows($result) > 0) {
        // Output table header
        echo "<div class='container'>
                <div class='table-responsive'>
                    <table class='table table-striped table-bordered'>
                        <thead class='thead-dark'>
                            <tr>
                            <th>Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Gender</th>
                                <th>CNIC</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>";

        // Output data of each row
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row["name"]. "</td>";
            echo "<td>" . $row["phone"]. "</td>";
            echo "<td>" . $row["address"]. "</td>";
            echo "<td>" . $row["gender"]. "</td>";
            echo "<td>" . $row["CNIC"]. "</td>";
            echo "<td>" . $row["amount"]. "</td>";
            echo "</tr>";
        }
        echo "</tbody></table></div></div>";
    } else {
        echo "No donor found";
    }

    // Close the connection
    mysqli_close($conn);
}

// Check if a specific view function is called
if(isset($_GET['action'])) {
    $action = $_GET['action'];
    switch($action) {
        case 'viewTotalBloodDonors':
            viewTotalBloodDonors();
            break;
        case 'viewTotalMoneyDonors':
            viewTotalMoneyDonors();
            break;
        case 'viewUsersBothDonors':
            viewUsersBothDonors();
            break;
        case 'viewSystemUsers':
            viewSystemUsers();
            break;
        case 'viewUsersNotBloodDonors':
            viewUsersNotBloodDonors();
            break;
        case 'viewUsersNotMoneyDonors':
            viewUsersNotMoneyDonors();
            break;
        default:
            // Handle invalid action
            echo "Invalid action!";
            break;
    }
}
?>
