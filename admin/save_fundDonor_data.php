
<?php

include('../conn.php');

function insertMoneyDonor($conn, $firstName, $lastName, $phone, $address, $gender, $cnic, $amount) {
    // Prepare the stored procedure call
    $stmt = mysqli_prepare($conn, "CALL Insert_contact_MoneyDonor(?, ?, ?, ?, ?, ?, ?, ?)");

    // Bind parameters
    mysqli_stmt_bind_param($stmt, 'ssssssdi', $firstName, $lastName, $phone, $address, $gender, $cnic, $amount, $user_id);

    // Execute the stored procedure
    $result1 = mysqli_stmt_execute($stmt);

    // Close the statement
    mysqli_stmt_close($stmt);


    // Check if both operations were successful
    if ($result1) {
        return true; // Success
    } else {
        return false; // Failure
    }
}

$firstName=$_POST['firstname'];
$lastName=$_POST['lastname'];
$phone=$_POST['phoneno'];
$email=$_POST['emailid'];
$cnic=$_POST['cnic'];
$gender=$_POST['gender'];
$amount=$_POST['amount'];
$address=$_POST['address'];
$conn=mysqli_connect("localhost","root","","1life") or die("Connection error");
if (insertMoneyDonor($conn, $firstName, $lastName, $phone, $address, $gender, $cnic, $amount)) {
    $message = "Record added successfully!";
    // You can redirect the user to a success page or display a success message here
} else {
    $error = "Error: Unable to add record.";
}
header("Location: funddonor_list.php");

mysqli_close($conn);
 ?>
