<?php
// Include database connection
include '../conn.php';

// Check if the donor_id is provided via GET request
if(isset($_GET['id'])) {
    // Sanitize the donor_id to prevent SQL injection
    $user_id = mysqli_real_escape_string($conn, $_GET['id']);

    // Construct the DELETE query
    $sql = "DELETE FROM blood_donors WHERE user_id = '$user_id'";
    $sql2 = "DELETE FROM contactinformation WHERE user_id = '$user_id'";
    $sql3 = "DELETE FROM medicalhistory WHERE user_id = '$user_id'";

    // Execute the DELETE query
    $result = mysqli_query($conn, $sql);
    $result2 = mysqli_query($conn, $sql2);
    $result3 = mysqli_query($conn, $sql3);



    // Execute the DELETE query
    if( $result && $result2 && $result3) {

        // Redirect back to the donor list page after successful deletion
        header("Location: blooddonor_list.php");
        
        exit;
    } else {
        // Display an error message if deletion fails
        echo "Error deleting record: " . mysqli_error($conn);
    }
}

// Close database connection
mysqli_close($conn);
?>
