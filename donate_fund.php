<?php
session_start();
$active = 'fund_donor';
include('head.php');
// Include database connection
include 'conn.php';

// Function to sanitize form data
function sanitize($conn, $data) {
    return mysqli_real_escape_string($conn, $data);
}

mysqli_close($conn);
?>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>

<body>
    <?php
    $active = 'fund_donor';

    // Check if user is already a donor
    if (isset($_SESSION['is_donor']) && $_SESSION['is_donor']) {
        
        // User is a donor, include CRUD_moneyDonor.php
        include('CRUD_moneyDonor.php');
    } else {
    ?>
    <div id="page-container" style="margin-top:50px; position: relative;min-height: 84vh;">
        <div class="container">
            <div id="content-wrap" style="padding-bottom:50px;">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="mt-4 mb-3">Donate Fund</h1>
                    </div>
                </div>
                
                <form id="donationForm" action="save_moneyDonor_data.php" method="post">
                    <div class="row">
                        <div class="col-lg-4 mb-4">
                            <div class="font-italic">First Name<span style="color:red">*</span></div>
                            <div><input type="text" class="form-control" name="firstName" required></div>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="font-italic">Last Name<span style="color:red">*</span></div>
                            <div><input type="text" class="form-control" name="lastName" required></div>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="font-italic">Phone #<span style="color:red">*</span></div>
                            <div><input type="text" class="form-control" name="phone" required></div>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="font-italic">Address<span style="color:red">*</span></div>
                            <div><input type="text" class="form-control" name="address" required></div>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="font-italic">Gender<span style="color:red">*</span></div>
                            <div>
                                <select class="form-control" name="gender" required>
                                    <option value="">Select</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Prefer Not to Say">Prefer Not to Say</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="font-italic">CNIC<span style="color:red">*</span></div>
                            <div><input type="text" class="form-control" name="CNIC" required></div>
                        </div>
                        <div class="col-lg-4 mb-4">
                            <div class="font-italic">Amount<span style="color:red">*</span></div>
                            <div>
                                <select class="form-control" name="amount" required>
                                    <option value="" selected disabled>Select</option>
                                    <?php
                                    include './admin/conn.php';
                                    $sql = "SELECT * FROM donation_amount";
                                    $result = mysqli_query($conn, $sql) or die("Query unsuccessful.");
                                    while($row = mysqli_fetch_assoc($result)) {
                                        echo "<option value='".$row['Amount']."'>".$row['Amount']."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-4 mb-4">
                            <div><input type="submit" class="btn btn-primary" value="Submit"></div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Popup Modal -->
            <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="loginModalLabel">Confirm Ation</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="loginForm">
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control" id="username" name="username"
                                            required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" name="password"
                                            required>
                                    </div>
                                    <button type="submit" class="btn btn-primary">OK</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <?php include('footer.php') ?>
    <!-- jQuery and Bootstrap JS
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <script>
    $(document).ready(function () {
        // Show the login modal when the donation form is submitted
        $('#donationForm').submit(function (event) {
            event.preventDefault(); // Prevent default form submission
            $('#loginModal').modal('show'); // Show the login modal
        });

        // Handle login form submission
        $('#loginForm').submit(function (event) {
            event.preventDefault(); // Prevent default form submission
            // Perform AJAX request to validate credentials
            $.ajax({
                url: 'validate_donor.php', // Your PHP script to validate credentials
                type: 'POST',
                data: $(this).serialize(),
                success: function (response) {
                    if (response === 'success') {
                        // If login is successful, submit the donation form
                        $('#donationForm').submit();
                    } else {
                        alert('Invalid username or password');
                    }
                },
                error: function (xhr, status, error) {
                    console.error(xhr.responseText);
                    alert('An error occurred while processing your request.');
                }
            });
        });
    });
    </script>

    <?php
    }
    ?> -->

    <!-- include('footer.php'); -->
</body>

</html>

