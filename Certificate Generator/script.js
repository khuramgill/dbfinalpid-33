var canvas = document.getElementById('canvas')
var ctx = canvas.getContext('2d')
var nameInput = document.getElementById('name')
var downloadBtn = document.getElementById('download-btn')

var image = new Image()
image.crossOrigin="anonymous";
image.src = 'certificate.png'
image.onload = function () {
	drawImage()
}

function drawImage() {
    // Clear the canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    
    // Draw the image
    ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
    
    // Set font and fill style
    ctx.font = '40px Edwardian Script ITC Regular';
    ctx.fillStyle = '#a30e26'; // Setting color
    
    // Calculate text width
    var textWidth = ctx.measureText(nameInput.value).width;
    
    // Calculate starting position for text
    var startX = (canvas.width - textWidth) / 2;
    var startY = 180; // Y-coordinate remains the same
    
    // Draw the text
    ctx.fillText(nameInput.value, startX, startY);
}


nameInput.addEventListener('input', function () {
	drawImage()
})

downloadBtn.addEventListener('click', function () {
	downloadBtn.href = canvas.toDataURL('image/jpg')
	downloadBtn.download = 'Certificate - ' + nameInput.value
    window.location.href = '../donate_fund.php';
})
