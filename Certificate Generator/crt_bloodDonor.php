<?php
session_start();
$active = 'blood_donor';
include('../head.php');
// Include database connection
include '../conn.php';

// Check if user ID is set in session
if(isset($_SESSION['id'])) {
    $user_id = $_SESSION['id'];
    
    // Fetch the name from the database based on user ID
    $stmt = $conn->prepare('SELECT bd.name as fullName FROM blood_donors as bd WHERE user_id = ?');
    $stmt->bind_param('i', $user_id); // 'i' indicates integer type
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();
    
    // Check if a row was returned
    if($row) {
        $name = $row['fullName'];
    } else {
        // Handle case where no row is returned
        $name = "User";
    }
} else {
    // Handle case where user ID is not set in session
    $name = "User";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <title>Blood Donor Certificate</title>
    <style>
        @font-face {
            font-family: 'Edwardian Script ITC Regular';
            src: url('./Edwardian Script ITC Regular.ttf') format('truetype');
        }

        .download-btn {
            display: inline-block;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s;
        }

        .download-btn:hover {
            background-color: #FFA500;
        }
    </style>
</head>

<body>
    <div class="container">
        <h1>Blood Donor Certificate</h1>
        <!-- Remove the input field and directly output the retrieved name -->
        <input id="name" style="display: none;" value="<?php echo $name; ?>">

        <a href="#" id="download-btn" class="download-btn">Download Certificate</a>
        <canvas id="canvas" height="350px" width="500px"></canvas>
    </div>
    <script>
        // Pass the name to the script.js file
        var name = "<?php echo $name; ?>";
    </script>
    <script src="script.js"></script>
</body>

</html>
