-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2024 at 04:46 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: 1life
--

-- --------------------------------------------------------

--
-- Table structure for table bloodtypes
--

CREATE TABLE bloodtypes (
  blood_type_id int(11) NOT NULL,
  blood_group varchar(5) NOT NULL,
  rh_factor varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table bloodtypes
--

INSERT INTO bloodtypes (blood_type_id, blood_group, rh_factor) VALUES
(1, 'B+', ''),
(2, 'B-', ''),
(3, 'A+', ''),
(4, 'O+', ''),
(5, 'O-', ''),
(6, 'A-', ''),
(7, 'AB+', ''),
(8, 'AB-', '');

-- --------------------------------------------------------

--
-- Table structure for table blood_donors
--

CREATE TABLE blood_donors (
  donor_id int(11) NOT NULL,
  name varchar(100) DEFAULT NULL,
  age int(11) DEFAULT NULL,
  gender varchar(10) DEFAULT NULL,
  blood_type_id int(11) DEFAULT NULL,
  contact_info_id int(11) DEFAULT NULL,
  medical_history_id int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table contactinformation
--

CREATE TABLE contactinformation (
  contact_info_id int(11) NOT NULL,
  user_id int(11) DEFAULT NULL,
  address varchar(255) DEFAULT NULL,
  phone varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table contactinformation
--

INSERT INTO contactinformation (contact_info_id, user_id, address, phone) VALUES
(45, 15, 'q', '03481512693'),
(46, 15, 'q', '03481512693'),
(47, 15, 'q', '03481512693'),
(48, 15, 'q', '03481512693'),
(49, 15, 'q', '03481512693'),
(50, 15, 'q', '03481512693'),
(51, 15, 'q', '03481512693'),
(52, 16, 'c', '03481512693');

-- --------------------------------------------------------

--
-- Table structure for table donationrecords
--

CREATE TABLE donationrecords (
  record_id int(11) NOT NULL,
  donation_id int(11) DEFAULT NULL,
  blood_type_id int(11) DEFAULT NULL,
  recipient_id int(11) DEFAULT NULL,
  date_received date DEFAULT NULL,
  expiry_date date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table donations
--

CREATE TABLE donations (
  donation_id int(11) NOT NULL,
  donor_id int(11) DEFAULT NULL,
  recipient_id int(11) DEFAULT NULL,
  donation_date date DEFAULT NULL,
  quantity_ml int(11) DEFAULT NULL,
  is_accepted bit(1) DEFAULT NULL,
  donationType int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table medicalhistory
--

CREATE TABLE medicalhistory (
  medical_history_id int(11) NOT NULL,
  history_description text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table moneydonor
--

CREATE TABLE moneydonor (
  donor_id int(11) NOT NULL,
  contact_info_id int(11) NOT NULL,
  firstName varchar(15) DEFAULT NULL,
  lastName varchar(15) DEFAULT NULL,
  gender int(11) DEFAULT NULL,
  amount varchar(20) DEFAULT NULL,
  CNIC int(11) DEFAULT NULL,
  transaction_id varchar(30) DEFAULT NULL,
  address varchar(50) NOT NULL,
  phone varchar(11) NOT NULL,
  user_id int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table moneydonor
--

INSERT INTO moneydonor (donor_id, contact_info_id, firstName, lastName, gender, amount, CNIC, transaction_id, address, phone, user_id) VALUES
(61, 0, 'q', 'q', 0, '5,000-10,000', 123, NULL, '', '', 15),
(62, 0, 'q', 'q', 0, '5,000-10,000', 123, NULL, '', '', 15),
(63, 0, 'q', 'q', 0, '5,000-10,000', 123, NULL, '', '', 15),
(64, 0, 'q', 'q', 0, '5,000-10,000', 123, NULL, '', '', 15),
(65, 0, 'q', 'q', 0, '5,000-10,000', 123, NULL, '', '', 15),
(66, 0, 'q', 'q', 0, '5,000-10,000', 123, NULL, '', '', 15),
(67, 0, 'q', 'q', 0, '5,000-10,000', 123, NULL, '', '', 15),
(68, 0, 'c', 'c', 0, '11,000-20,000', 23456, NULL, '', '', 16);

-- --------------------------------------------------------

--
-- Table structure for table recipients
--

CREATE TABLE recipients (
  recipient_id int(11) NOT NULL,
  name varchar(100) DEFAULT NULL,
  age int(11) DEFAULT NULL,
  gender varchar(10) DEFAULT NULL,
  blood_type_id int(11) DEFAULT NULL,
  contact_info_id int(11) DEFAULT NULL,
  medical_history_id int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table transactions
--

CREATE TABLE transactions (
  transaction_id int(11) NOT NULL,
  donation_id int(11) DEFAULT NULL,
  amount decimal(10,2) DEFAULT NULL,
  transaction_date date DEFAULT NULL,
  transaction_type varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table users
--

CREATE TABLE users (
  id int(11) NOT NULL,
  username varchar(50) DEFAULT NULL,
  password varchar(50) DEFAULT NULL,
  email varchar(30) DEFAULT NULL,
  role varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table users
--

INSERT INTO users (id, username, password, email, role) VALUES
(15, 'q', '8e35c2cd3bf6641bdb0e2050b76932cbb2e6034a0ddacc1d9b', 'q@gmail.com', 'user'),
(16, 'c', '2e7d2c03a9507ae265ecf5b5356885a53393a2029d24139499', 'c@gmail.com', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table bloodtypes
--
ALTER TABLE bloodtypes
  ADD PRIMARY KEY (blood_type_id);

--
-- Indexes for table blood_donors
--
ALTER TABLE blood_donors
  ADD PRIMARY KEY (donor_id),
  ADD KEY FK_Donors_BloodTypes (blood_type_id),
  ADD KEY FK_Donors_ContactInformation (contact_info_id),
  ADD KEY FK_Donors_MedicalHistory (medical_history_id);

--
-- Indexes for table contactinformation
--
ALTER TABLE contactinformation
  ADD PRIMARY KEY (contact_info_id),
  ADD KEY fk_user_id (user_id);

--
-- Indexes for table donationrecords
--
ALTER TABLE donationrecords
  ADD PRIMARY KEY (record_id),
  ADD KEY FK_DonationRecords_BloodTypes (blood_type_id),
  ADD KEY FK_DonationRecords_Donations (donation_id),
  ADD KEY FK_DonationRecords_Recipients (recipient_id);

--
-- Indexes for table donations
--
ALTER TABLE donations
  ADD PRIMARY KEY (donation_id),
  ADD KEY FK_Donations_MoneyDonor (donor_id);

--
-- Indexes for table medicalhistory
--
ALTER TABLE medicalhistory
  ADD PRIMARY KEY (medical_history_id);

--
-- Indexes for table moneydonor
--
ALTER TABLE moneydonor
  ADD PRIMARY KEY (donor_id);

--
-- Indexes for table recipients
--
ALTER TABLE recipients
  ADD PRIMARY KEY (recipient_id);

--
-- Indexes for table transactions
--
ALTER TABLE transactions
  ADD PRIMARY KEY (transaction_id),
  ADD KEY FK_Transactions_Donations (donation_id);

--
-- Indexes for table users
--
ALTER TABLE users
  ADD PRIMARY KEY (id);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table bloodtypes
--
ALTER TABLE bloodtypes
  MODIFY blood_type_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table contactinformation
--
ALTER TABLE contactinformation
  MODIFY contact_info_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table moneydonor
--
ALTER TABLE moneydonor
  MODIFY donor_id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table users
--
ALTER TABLE users
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table blood_donors
--
ALTER TABLE blood_donors
  ADD CONSTRAINT FK_Donors_BloodTypes FOREIGN KEY (blood_type_id) REFERENCES bloodtypes (blood_type_id),
  ADD CONSTRAINT FK_Donors_ContactInformation FOREIGN KEY (contact_info_id) REFERENCES contactinformation (contact_info_id),
  ADD CONSTRAINT FK_Donors_MedicalHistory FOREIGN KEY (medical_history_id) REFERENCES medicalhistory (medical_history_id);

--
-- Constraints for table contactinformation
--
ALTER TABLE contactinformation
  ADD CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (id);

--
-- Constraints for table donationrecords
--
ALTER TABLE donationrecords
  ADD CONSTRAINT FK_DonationRecords_BloodTypes FOREIGN KEY (blood_type_id) REFERENCES bloodtypes (blood_type_id),
  ADD CONSTRAINT FK_DonationRecords_Donations FOREIGN KEY (donation_id) REFERENCES donations (donation_id),
  ADD CONSTRAINT FK_DonationRecords_Recipients FOREIGN KEY (recipient_id) REFERENCES recipients (recipient_id);

--
-- Constraints for table donations
--
ALTER TABLE donations
  ADD CONSTRAINT FK_Donations_Donors FOREIGN KEY (donor_id) REFERENCES blood_donors (donor_id),
  ADD CONSTRAINT FK_Donations_MoneyDonor FOREIGN KEY (donor_id) REFERENCES moneydonor (donor_id);

--
-- Constraints for table transactions
--
ALTER TABLE transactions
  ADD CONSTRAINT FK_Transactions_Donations FOREIGN KEY (donation_id) REFERENCES donations (donation_id);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;